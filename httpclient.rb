require 'js'
require 'console'


require 'javascriptproxy'

module HTTP
  class IncomingMessage < OBx
    proxy_attr_reader :url
  end
  class ServerResponse < OBx
    proxy_method :end
    proxy_attr_accessor :statusCode
    proxy_attr_accessor :statusMessage
  end
end

# Lets require/import the module
#http = `require('http')`
#fs = `require('fs')`

=begin
require 'net/http'

url = URI.parse('http://localhost:8080/svg/bezier/bezieredit_2.html')
req = Net::HTTP::Get.new(url.to_s)
res = Net::HTTP.start(url.host, url.port) {|http|
  http.request(req)
}
puts res.body
=end

%x{
  var http = require('http');
  var str = '';

  var options = {
        host: 'localhost',
        port: 8080,
        path: '/svg/bezier/bezieredit_2.html'
  };
  callback = function(response) {

    response.on('data', function (chunk) {
      str += chunk;
    });

    response.on('end', function () {
      console.log(req.data);
      console.log(str);
      // your code here if you want to use the results !
    });
  }

  var req = http.request(options, callback)
  req.end();

}
