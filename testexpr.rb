#
require 'rbox'
require 'console'

class ExpressApp < RBox
  include ProxyMethodMissing
end

def require_express
  express = `require('express')`
  ->() do
    `express()`
  end
end

express = require_express

app = ExpressApp.new(express.())

response = ""
app.all '/', -> (req, res, next_route) do
  response = 'Hello World from all!<br>'
  next_route.JS.call()
end
app.get '/', -> (req, res) do
  response += 'Hello World from get!<br>'
  res.JS.send(response);
end
server = app.listen(3000, -> {
  host = server.JS.address().JS['address']
  port = server.JS.address().JS['port']

  $console.log('Example app listening at http://%s:%s', host, port)
})
