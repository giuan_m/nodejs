
require 'js'

require 'rbox'

dns = `require('dns');`

$console = RBox(`console`)
class << $console
  proxy_method :log
end

$console.log("calling dns")

class Dns < RBox
  proxy_method :lookup
end
dns = Dns.new(dns)
dns.lookup('www.google.com', ->(err, addresses, family) {
  $console.log('addresses:', RBox(addresses).to_s, ' ', family.JS.toString);
})
