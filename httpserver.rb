require 'js'
require 'console'


require 'rbox'

module HTTP
  class IncomingMessage < RBox
    proxy_attr_reader :url
  end
  class ServerResponse < RBox
    proxy_method :end
    proxy_attr_accessor :statusCode
    proxy_attr_accessor :statusMessage
  end
end

# Lets require/import the module
http = `require('http')`
fs = `require('fs')`

#Lets define a port we want to listen to
PORT=8080

# We need a function which handles requests and send response
handle_request = ->(request, response) do
  req = HTTP::IncomingMessage.new request
  resp = HTTP::ServerResponse.new response
  $console.log('* It Works!! Path Hit: ' + req.url)
  fs.JS.readFile './'+req.url, ->(err, data) do
    if (err)
      resp.statusCode = 404
      #resp.statusMessage = "File not found"
      resp.end("Sorry file not found")
    else
      resp.end(data)
    end
  end
end

# Create a server
httpserver = http.JS.createServer(handle_request)

# Lets start our server
httpserver.JS.listen(PORT, ->(){
    # Callback triggered when server is successfully listening. Hurray!
    $console.log("Server listening on: http://localhost:#{PORT}")
})


child_process = `require('child_process')`
url = "http://localhost:8080/svg/bezier/bezieredit.html"
cmd = "\"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome\" --app=#{url}"
#cmd = "\"C:/Program Files (x86)/Google/Chrome/Application/chrome.exe\" --app=#{url}"
child_process.JS.exec cmd, ->(error, stdout, stderr) do
    $console.log(error)
end

->() do

fs.JS.watch('.', -> (event, filename) {
  puts('event is: ' + event)
  if (filename)
    $console.log('filename provided: ' + filename)
  else
    $console.log('filename not provided')
  end
});
end
