require 'js'

fs = `require('fs')`

# dir but no subdir
->() do
  fs.JS.watch('./', {persistent: true, recursive: true}, -> (event, filename) {
    puts('event is: ' + event)
    if (filename)
      puts('filename provided: ' + filename)
    else
      puts('filename not provided')
    end
  });
end.call
