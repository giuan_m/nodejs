# my_rack_app.rb

if RUBY_ENGINE == 'opal'
  puts "running with opal"
  require 'opal'
  require_relative 'jazz'
  require_relative 'jazz_opal'
else
  puts "running with ruby"
  require 'rack'
  require_relative 'jazz'
  require_relative 'jazz_ruby'
end


app = JazzApp.new

#pp.add_middleware([Rack::Static, :urls => ['/static'], :root => './'])

app.get "/cors", ->(request, response) do
  response.set_header("Access-Control-Allow-Origin","*")
  response.write("cross domain")
end

app.listen(8080, "192.168.0.4")
