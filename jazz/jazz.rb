class RouterNode
  attr_accessor :type, :path, :path_parameters, :handler_or_route, :next_node
  def initialize type = nil, path = nil, handler_or_route = nil
    @type = type  # GET, ..., ALL, ROUTE
    @path = path
    @path_parameters = [] # *, :id
    @handler_or_route = handler_or_route  # element | RouterNode
    @next_node = nil
  end
end

class JazzAppCommon
  CONTINUE = Object.new
  def initialize
    @root = RouterNode.new  # empty last node
    @last_node = @root
    @route_path = ""
    @middleware = []
  end

  def continue
    return CONTINUE
  end

  def urlpath_to_regexp path, end_of_string = true
    r = "^"
    path_parameters = []
    while path != ""
      i = path.index(/[*:]/)
      if i
        r += Regexp.escape(path[0..i-1]) if i > 0
        c = path[i]
        path = path[(i+1)..-1]
        if c == '*'
          r += "(.*)"
          path_parameters << "*"
        else
          j = path.index('/') || 0
          path_parameters << path[0..(j-1)]
          r += "([^\/]+)"
          path = path[j..-1]
        end
      else
        r += path
        path = ""
      end
    end
    r += "$" if end_of_string
    return Regexp.new(r), path_parameters
  end

  def add_middleware middleware  # [middleware, arg1, ...]
    @middleware << middleware
  end

  def http_method(method, path, handler)
    @last_node.type = method
    if method == "ROUTE"
      @last_node.path = path  # route are string
    elsif path.is_a? Regexp
      @last_node.path = path
    else
      if path == ""
        path = @route_path.chop  # no last '/'
      else
        path = @route_path + path
      end
      @last_node.path, @last_node.path_parameters = urlpath_to_regexp path
    end
    @last_node.handler_or_route = handler
    n = RouterNode.new
    @last_node.next_node = n
    @last_node = n
  end

  def get(path, handler)
    http_method("GET", path, handler)
  end
  def post(path, handler)
    http_method("POST", path, handler)
  end
  def put(path, handler)
    http_method("PUT", path, handler)
  end
  def delete(path, handler)
    http_method("DELETE", path, handler)
  end
  def all(path, handler)
    http_method("ALL", path, handler)
  end
  def route(path, route_fun)
    # insert an empty child node
    n =  RouterNode.new
    http_method("ROUTE", path, n)
    # new route path
    p_route_path = @route_path
    if path.start_with?('/')
      @route_path = path + '/'
    else
      @route_path += path + '/'
    end
    p_last_node = @last_node  # empty node sibling
    @last_node = n # the empty children become the last node
    # insert handler as child
    route_fun.call
    # end of child, sibling now
    @last_node = p_last_node # next sibling
    @route_path = p_route_path
  end

  def visit node, fun, ident = ""
    while node.type
      fun.call(node, ident)
      if node.type == "ROUTE"
        visit(node.handler_or_route, fun, ident + "  ")
      end
      node = node.next_node
    end
  end


  def handle_request(node, request, response)
    found = false
    while node.type
      if node.type == request.request_method || node.type == "ALL"
        m = node.path.match(request.path_info)
        if m
          captures = []
          m.captures.each_with_index do |c, i|
            name = node.path_parameters.size > 0 ? node.path_parameters[i] : "*"
            if name == "*"
              captures << c
            else
              request.params[name] = c
            end
          end
          request.params["captures"] = captures
          found = node.handler_or_route.call(request, response) != CONTINUE
        end
      elsif node.type == "ROUTE" && request.path_info.start_with?(node.path)
        found = handle_request(node.handler_or_route, request, response)
      end
      return true if found
      node = node.next_node
    end
    found
  end

end
