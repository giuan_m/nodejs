# myapp.rb
require 'sinatra'

enable :sessions
use Rack::Static, :urls => ["/static"]
class Dati
  attr_accessor :nome, :cognome, :s
  def initialize
    @nome = ""
    @cognome = ""
    @s = "a"*1000
  end
end

get "/login" do
  puts "login"
  d = Dati.new
  d.cognome = 'Rossi'
  request.session['dati'] = d
  response.redirect('/index')
end

get "/logout" do
  puts "logout"
  request.session.clear
end

get "*"  do
  puts "*"
  d = request.session['dati']
  if d && d.cognome
    pass
  else
    response.redirect('/login')
  end
end

get "/index" do
  d = request.session['dati']
  puts "Cognome: #{d.cognome}"
  response.write("this is the index")
  pass
end

get "/index" do
  response.write("<br>this is the second index")
end

get "/artisti" do
    response.write("this is the inserisci")
end
get "/artisti/modifica" do
    response.write("this is the modifica")
end
