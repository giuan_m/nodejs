# my_rack_app.rb

if RUBY_ENGINE == 'opal'
  puts "running with opal"
  require 'opal'
  require_relative 'jazz'
  require_relative 'jazz_opal'
else
  puts "running with ruby"
  require 'rack'
  require_relative 'jazz'
  require_relative 'jazz_ruby'
end


class Dati
  attr_accessor :nome, :cognome, :s
  def initialize
    @nome = ""
    @cognome = ""
    @s = ""
  end
end

app = JazzApp.new

app.add_middleware([Rack::Static, :urls => ['/static'], :root => './'])

app.get "/login", ->(request, response) do
  d = Dati.new
  d.cognome = 'Rossi'
  request.session['dati'] = d
  response.redirect('/index')
end

app.get "/logout", ->(request, response) do
  request.session.clear
end

app.get "*", ->(request, response) do
  d = request.session['dati']
  if d && d.cognome
    app.continue
  else
    response.redirect('/login')
  end
end

app.get "/index", ->(request, response) do
  d = request.session['dati']
  response.write("this is the index")
  app.continue
end

app.get "/index", ->(request, response) do
  response.write("<br>this is the second index")
end

app.route "/artisti", ->() do
  app.get "", ->(request, response) do
    response.write("this is the inserisci")
  end
  app.get "modifica", ->(request, response) do
    response.write("this is the modifica")
  end
end

app.get "/stream", ->(request, response) do
  response.stream_thread request.env,  ->(io) do
    html = %{
      <html>
        <head>
        </head>
      <body>
    }
    io.write(html)
    n = 10
    times_with_sleep n, 1,
      ->() do
        io.write "#{Time.now}<br>"
        io.flush
      end,
      # finally
      -> () do
        io.write("</body></html>")
        io.flush
        io.close
      end
  end
end

app.get "/ajax/stream", ->(request, response) do
  response.stream ->(io) do
    n = 10
    times_with_sleep n, 1,
      ->() do
        io.write "#{Time.now}<br>"
        io.flush
      end,
      # finally
      -> () do
        io.close
      end
  end
end

app.listen(8080, "localhost")
