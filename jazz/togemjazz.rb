require 'fileutils'

def find_gem_path_gem name
  gem_path = Gem.path.find do |base|
    gem_path = $LOAD_PATH.find do |path|
      gem_path = path[%r{#{base}/(bundler/)?gems/#{name}\-[^/-]+/}]
      break gem_path if gem_path
    end
    break gem_path if gem_path
  end
  gem_path[0...-1] if gem_path
end

def find_gem_path name

  # find the exact gem
  if gem_path = find_gem_path_gem(name)
    return gem_path

  # find the require file
  elsif path = Gem.find_files("#{name}.rb").first
    # favor gem first (e.g. rake gem)
    if gem_path = Gem.path.find{ |p|
         break $1 if path =~ %r{(#{p}(/[^/]+){2})}
       }
      return gem_path
    else
      return path
    end
  else
    return nil
  end
end

def copy_to_gem file_to_cp
  path = find_gem_path('opal') + '/stdlib'
  if path
    FileUtils.cp file_to_cp, path
    puts "File #{file_to_cp} copied to #{path}"
  else
    puts("#{file_to_cp} not found")
  end
end

copy_to_gem 'jazz.rb'
copy_to_gem 'jazz_opal.rb'
