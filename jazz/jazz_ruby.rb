require "rack/handler/puma"

def log msg
  puts msg
end

def handle_request_file (request, response)
  data = File.read('./'+request.path_info)
  response.write(data)
end

class ChunckedIo
  def initialize io
    @io = io
    @buff = ""
    @closed = false
  end
  def write str
    @buff << str
  end
  def flush
    n = Rack::Utils.bytesize(@buff)
    @io.write("#{n.to_s(16)}\r\n")
    @io.write(@buff) if n > 0
    @io.write("\r\n")
    @io.flush
    @buff = ""
  end
  def close
    flush if @buff.length > 0
    @io.write("0\r\n\r\n")
    @io.flush
    @closed = true
    @io.close
  end
  def closed?
    @closed
  end
end

class Rack::Response
  def stream_thread env, handler, content_type = "text/html"
    response_headers = {}
    response_headers["Content-Type"] = content_type
    response_headers["Transfer-Encoding"] = "chunked"
    env['rack.hijack'].call
    io = env['rack.hijack_io']
    io.write("HTTP/1.1 200 Ok\r\n")
    io.write("Connection: close\r\n")
    io.write("Transfer-Encoding: chunked\r\n")
    io.write("Content-Type: #{content_type}\r\n")
    io.write("\r\n")
    io.flush
    io = ChunckedIo.new io
    Thread.new do
      begin
         handler.call(io)
      ensure
         io.close unless io.closed?
      end
    end
    @header = {}
    @status = 200
    @body = nil
    true
  end

  def stream handler, content_type = "text/html"
    response_headers = {}
    response_headers["Content-Type"] = content_type
    response_headers["Transfer-Encoding"] = "chunked"
    response_headers["rack.hijack"] = ->(io) do
      io = ChunckedIo.new io
      handler.call(io)
      io.close unless io.closed?
    end
    @header = response_headers
    @status = 200
    @body = nil
    true
  end
end

class JazzApp < JazzAppCommon
  def initialize
    super()
    # create server
    @app = ->(env) do
      puts "Thread #{Thread.current}"
      request = Rack::Request.new(env)
      response = Rack::Response.new
      log(request.path_info)
      if !handle_request(@root, request, response)
        response.write("No handle for #{request.url}")
      end
      triple = response.finish  # return the generated triplet
      triple
    end

  end

  def listen port=8080, host="localhost"
    visit @root, ->(n, ident) do
      puts("#{ident}#{n.type} #{n.path}")
    end
    # Lets start our server
    # Create a server
    myapp = @app
    middleware = @middleware
    app = Rack::Builder.app() do
      use Rack::Chunked
      use Rack::Session::Pool, :key => 'session_id'
      middleware.each {|m| use *m }
      run myapp
    end
    #Rack::Handler::WEBrick.run app, :Port => port
    Rack::Handler::Puma.run app, :Host => host, :Port => port
  end
end

def times_with_sleep (n, delay, block, final_block)
  return if n <= 0
  n.times do
    sleep(delay)
    block.call
  end
  final_block.call
end
