pattern = '/foo/*/:nome/:c/foo/:x/foo.*'
path = '/foo/bar/giovanni/x/foo/fede/foo.bat'

def urlpath_to_regexp path, end_of_string = true
  r = "^"
  captures_names = []
  while path != ""
    i = path.index(/[*:]/)
    if i
      r += Regexp.escape(path[0..i-1])
      c = path[i]
      path = path[(i+1)..-1]
      if c == '*'
        r += "(.*)"
        captures_names << "*"
      else
        j = path.index('/') || 0
        captures_names << path[0..(j-1)]
        r += "([^\/]+)"
        path = path[j..-1]
      end
    else
      r += path
      path = ""
    end
  end
  r += "$" if end_of_string
  return Regexp.new(r), captures_names
end

r, names = urlpath_to_regexp pattern

puts r.source
puts names

m = r.match path

if m
    puts m.captures
else
    puts "No match"
end
