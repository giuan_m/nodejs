require 'opal'
require 'opal-jquery'
require 'racc/parser'


File.open('opal.js', 'wt') { |file| file.puts(Opal::Builder.build('opal')) }
File.open('opal-parser.js', 'wt') { |file| file.puts(Opal::Builder.build('opal-parser')) }
File.open('opal-jquery.js', 'wt') { |file| file.puts(Opal::Builder.build('opal-jquery')) }
File.open("math.js", 'wt') { |file| file.write(Opal::Builder.build('math')) }
File.open("js.js", 'wt') { |file| file.write(Opal::Builder.build('js')) }
File.open("native.js", 'wt') { |file| file.write(Opal::Builder.build('native')) }
File.open("json.js", 'wt') { |file| file.write(Opal::Builder.build('json')) }
