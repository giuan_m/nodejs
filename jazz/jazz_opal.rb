require 'js'
require_relative 'lib/rbox'
require_relative 'jazz'
require 'securerandom'

def set_timeout(callback,delay)
  `setTimeout(callback,delay)`
end
def log msg
  puts msg
end
module HTTP
  class IncomingMessage < RBox
    proxy_attr_reader :path_info, :url
    proxy_attr_reader :request_method, :method
    attr_reader :params
    attr_accessor :session
    attr_reader :env
    def initialize  j
      super j
      @params = {}
      @session = nil
      @env = nil
    end
  end
  class ServerResponse < RBox
    proxy_method :finish, :end
    proxy_method :write
    proxy_method :set_header, :setHeader
    proxy_attr_accessor :status, :statusCode

    #proxy_attr_accessor :statusMessage
    def initialize  j, app
      super j
      @app = app
      @chunked = false
    end

    def redirect url
      @app.redirect url
    end
    # rack hijeck
    def chunked?
      @chunked
    end

    def flush
    end
    def close
      finish
    end

    def stream_thread env, handler, content_type = "text/html"
      stream handler, content_type
    end

    def stream handler, content_type = "text/html"
      @chunked = true
      set_header("Content-Type", content_type)
      set_header('Transfer-Encoding', 'chunked')
      handler.call(self)
      true
    end
  end
end

# Lets require/import the module
$http = `require('http')`
$fs = `require('fs')`


## middleware
#   rule: middleware must be sync or if async handle the request
#         and call response.finish

module Rack
  class Static
    def initialize params
      @urls = params[:urls]
      @root = params[:root]
    end
    def  handle (request, response)
      path = request.path_info
      path_static = nil
      for i in 0..(@urls.size-1)
        if path.start_with? @urls[i]
          path_static = path
          break
        end
      end
      if path_static
        data = $fs.JS.readFileSync @root+path_static
        if (data)
          response.write(data)
        else
          response.status = 404
          #response.statusMessage = "File not found"
          response.write("Sorry file not found")
        end
        response.finish
        true
      else
        false
      end
    end
  end

  class StaticAsynch
    def initialize params
      @urls = params[:urls]
      @root = params[:root]
    end
    def handle (request, response)
      path = request.path_info
      path_static = nil
      for i in 0..(@urls.size-1)
        if path.start_with? @urls[i]
          path_static = path
          break
        end
      end
      return false unless path_static
      data = $fs.JS.readFile  @root+path_static, ->(err, data) do
        if (err)
          response.status = 404
          #response.statusMessage = "File not found"
          response.write("Sorry file not found")
        else
          response.write(data)
        end
        response.finish
      end
      true
    end
  end
end

# =begin
# var querystring = require('querystring');
#
# function processPost(request, response, callback) {
#     var queryData = "";
#     if(typeof callback !== 'function') return null;
#
#     if(request.method == 'POST') {
#         request.on('data', function(data) {
#             queryData += data;
#             if(queryData.length > 1e6) {
#                 queryData = "";
#                 response.writeHead(413, {'Content-Type': 'text/plain'}).end();
#                 request.connection.destroy();
#             }
#         });
#
#         request.on('end', function() {
#             request.post = querystring.parse(queryData);
#             callback();
#         });
#
#     } else {
#         response.writeHead(405, {'Content-Type': 'text/plain'});
#         response.end();
#     }
# }
# =end
$sessions = {}

class JazzApp < JazzAppCommon
  def initialize
    super.initialize
    # create server
    native_handle_request = ->(request, response) do
      headers = request.JS["headers"]
      cookie = headers.JS['cookie']
      puts request.JS['url']
      # puts "cookie: #{cookie}"
      if `typeof cookie == "undefined"`
        cookie = nil
      end
      session = nil
      if cookie
        sids = cookie.scan(/sid\=[^;]*/)
        sid = nil
        sids.each do |e|
          e = e.split('=')[1]
          x = $sessions[e]
          if x != nil
            session = x
            break
          end
        end
      end
      if !(session)
        sid = SecureRandom.uuid
        response.JS.setHeader("Set-Cookie", "sid=#{sid};path=/")
        $sessions[sid] = session = {}
      end
      @original_request = request
      @original_response = response
      request = HTTP::IncomingMessage.new request
      response = HTTP::ServerResponse.new response, self
      request.session = session
      #log(request.path_info)
      r = false
      @middleware.each { |m|
        r = m.handle request, response
        # return true if the handler call response.finish
        break if r
      }
      unless r
        if !handle_request(@root, request, response)
          response.write("No handle for #{request.path_info}")
        end
        response.finish unless response.chunked?
      end
    end

    # Create a server
    @httpserver = $http.JS.createServer(native_handle_request)

  end

  def redirect url, status=302
    request = @original_request
    response = @original_response
    %x{
      urlre = (request.socket.encrypted ? 'https://' : 'http://') +
              request.headers.host + url
      console.log("redirect " + urlre)
      response.writeHead(status, {
        Location: urlre
      }
      );
    }
    true
  end

  def listen port, hostname = "localhost"
    visit @root, ->(n, ident) do
      puts("#{ident}#{n.type} #{n.path}")
    end
    middleware = []
    @middleware.each {|m| middleware << m[0].new(*m[1..-1])}
    @middleware = middleware
    # Lets start our server
    @httpserver.JS.listen port, hostname, ->() do
        # Callback triggered when server is successfully listening. Hurray!
        puts("Jazz listening on: http://#{hostname}:#{port}")
    end
  end
end

def times_with_sleep (n, delay, block, final_block)
  return nil if n <= 0
  callback = ->() do
    block.call
    n -= 1
    if n > 0
      set_timeout(callback, delay*1000)
    elsif final_block
      final_block.call
    end
  end
  set_timeout(callback, delay*1000)
  nil
end

# We need a function which handles requests and send response
def handle_request_file (request, response)
  $fs.JS.readFile './'+request.path_info, ->(err, data) do
    if (err)
      response.status = 404
      #response.statusMessage = "File not found"
      response.write("Sorry file not found")
    else
      response.write(data)
    end
    response.finish
  end
end
