f2 = nil
f1 = ->() do
  puts "f1"
  f2.()
end

f2 = ->() do
  puts "f2"
end


f1.()

def a
  yield "ok"
end

def b f
  f.("ok")
end

f = -> (p) {puts "lambda #{p}"}

puts (b -> (p) {puts "lambda #{p}"})
puts b(f)
