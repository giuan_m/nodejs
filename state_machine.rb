require 'js'

$fs = `require('fs')`

fname = 'state_machine.rb'
fname2 = 'state_machine.js'

state_machine = ->(state, *args) do
  case state
  when :exists?
    $fs.JS.exists fname, ->(exists) do
      if exists
        state_machine.(:read_file)
      else
        `console`.JS.log('no file!')
      end
    end
  when :read_file
      $fs.JS.readFile fname, -> (err, data) do
        if err
          `console`.JS.log('error reading file')
        else
          `console`.JS.log(data.JS.toString)
          state_machine.(:exists_2?)
        end
      end
    when :exists_2?
      $fs.JS.exists fname, ->(exists) do
        if exists
          state_machine.(:read_file2)
        else
          `console`.JS.log('no file!')
        end
      end
    when :read_file2
      $fs.JS.readFile fname, -> (err, data) do
        if err
          `console`.JS.log('error reading file')
        else
          `console`.JS.log(data.JS.toString)
        end
      end
    end
end

#state_machine.call(:exists?)


class AsyncJob
  def initialize name, fun
    @fun_chain = [fun]
    @call_n = -1
  end

  def step name, fun
    @fun_chain << fun
    self
  end

  def next_call name, *params
    @call_n += 1
    @fun_chain[@call_n].call(*params)
  end

  def run
    next_call
  end

end

class AsyncJobTm
  def initialize
    @fun_chain = {}
  end

  def step name, fun=nil, &block
    @fun_chain[name] = fun ? fun : block
    self
  end

  def next_call name, *params
    begin
      @fun_chain[name].call(*params)
    rescue Exception => e
      f = @fun_chain["rescue"]
      f.call(e) if f
    end
  end

  def run
    next_call @fun_chain.first[0]
  end
end

class Fs
  def initialize fs
    @fs = fs
  end
  def exists? (fname, job, next_step=nil)
    @fs.JS.exists fname, ->(exists) {job.next_call(next_step, exists)}
  end
  def readfile (fname, job, next_step=nil)
    @fs.JS.readFile fname, -> (err, data) {job.next_call(next_step, err, data)}
  end
end

fname = 'state_machine.rb'
fname2 = 'state_machine.js'

fs = Fs.new $fs
work = AsyncJobTm.new
work.step "1", ->() do
  fs.exists?(fname,work,"2")
end
work.step "2", ->(exists) do
  if exists
    fs.readfile(fname, work,"3")
  end
end
work.step "3", -> (err, data) do
  if err
    raise "Error reading #{fname}"
  else
    `console`.JS.log(data.JS.toString.length)
    fs.exists?(fname2, work, "4")
  end
end
work.step "4", ->(exists) do
  if exists
    `console`.JS.log(fname2)
    fs.readfile(fname2, work, "5")
  end
end
work.step "5", -> (err, data) do
  if err
    `console`.JS.log('error reading file')
  else
    `console`.JS.log(data.JS.toString.length)
  end
end
work.step "rescue", -> (e) do
  `console`.JS.log(e)
end

work = AsyncJobTm.new
work.step "1"  do
  fs.exists?(fname,work,"2")
end
work.step "2"  do |exists|
  if exists
    fs.readfile(fname, work,"3")
  end
end
work.step "3" do  |err, data|
  puts err
  if err
    raise "Error reading #{fname}"
  else
    `console`.JS.log(data.JS.toString.length)
    fs.exists?(fname2, work, "4")
  end
end
work.step "4", ->(exists) do
  if exists
    `console`.JS.log(fname2)
    fs.readfile(fname2, work, "5")
  end
end
work.step "5", -> (err, data) do
  if err
    `console`.JS.log('error reading file')
  else
    `console`.JS.log(data.JS.toString.length)
  end
end
work.step "rescue", -> (e) do
  `console`.JS.log(e)
end

work.run
